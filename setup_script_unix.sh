#!/bin/bash

#    setup_script.sh <-- must be executable to be called from terminal
#    purpose: Trims xml databases by using the xml score attribute, finally it will setup the stackexchange server for you.
#    @author Teoman Reimann, Germany
#    @version 2.0 28/07/19 
#    requirements: https://bitbucket.org/samuel.lai/stackdump, wget, g++ compiler,7z
#    tested with Debian based distro: Linux Mint 19.1
#    License: MIT License inherited from samuel.lai/stackdump

# Some color output to mark success
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
NC='\033[0m' # No Color
#create dumpFolder
mkdir ./dumpData
mkdir ./dumpData/outputs
#move trimming script into dumpData directory
mv ./CommentFilter.cpp ./dumpData
mv ./PostFilter.cpp ./dumpData
mv ./UserFilter.cpp ./dumpData
sleep 1s
#download,unpack xml,delete zip
printf "${YELLOW}Downloading database from archive.org...${NC} \n"
wget -P ./dumpData https://archive.org/download/stackexchange/unix.stackexchange.com.7z
printf "${YELLOW}Unpacking database...${NC} \n"
7z e -o./dumpData ./dumpData/unix.stackexchange.com.7z
printf "${YELLOW}Remove useless xml files and zip file...${NC} \n"
cd dumpData
rm Badges.xml
rm PostHistory.xml
rm PostLinks.xml
rm Tags.xml
rm Votes.xml
rm unix.stackexchange.com.7z
mv Comments.xml Comments_src.xml
mv Posts.xml Posts_src.xml
mv Users.xml Users_src.xml
sleep 1s
#compiling...
printf "${YELLOW}Compiling trimming script...${NC} \n"
g++ -O2 CommentFilter.cpp -o CommentFilter.o
g++ -O2 PostFilter.cpp -o PostFilter.o
g++ -O2 UserFilter.cpp -o UserFilter.o
#trimming
./CommentFilter.o -score=2
./PostFilter.o -score=3 -viewcount=3000 -favcount=2
./UserFilter.o -reputation=2
rm -rd outputs
sleep 1s
printf "${YELLOW}Cleaning up...${NC} \n"
rm Comments_src.xml
rm Posts_src.xml
rm Users_src.xml
rm CommentFilter.o
rm PostFilter.o
rm UserFilter.o
cd ..
sleep 1s
#download additional data from stackexchange.com
printf "${YELLOW}Download site info...${NC} \n"
./manage.sh download_site_info
sleep 1s
printf "\n\n"
printf "${YELLOW}For future reference:${NC} \n"
printf "${YELLOW}start solr with ./start_solr.sh${NC} \n"
printf "${YELLOW}dateVar=\`date +\"%m %Y\"\`${NC} \n"
printf "${YELLOW}./manage.sh import_site --base-url tex.stackexchange.com --dump-date "$dateVar" ./dumpData${NC} \n"
printf "${YELLOW}start web-interface with /start_web.sh${NC} \n"
printf "${YELLOW}visit server on localhost:8080${NC} \n"
printf "\n${GREEN}\u2713 setup was successful!${NC} \n"
