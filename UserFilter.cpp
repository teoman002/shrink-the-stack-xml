/**
    UsersFilter.cpp
    Purpose: Trims Users.xml by using the xml attribute "reputation". All entries with a reputation smaller than the passed parameter will be omitted and their "ID" will be written into a seperate file.

    @author Teoman Reimann
    @version 1.0 20/07/19
    @arguments  -reputation=NUM
    
*/

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <chrono> 
#include <unistd.h>

using namespace std::chrono; 
using namespace std;

const string inFileName = "./Users_src.xml";
const string trimmed_users_path = "./Users.xml";
const string deleted_ids_path = "outputs/Users_deleted_ids.xml";
const string delim = "\" "; //used to split xml attributes

/**
	@brief  shows a dynamic progress bar in the terminal
	@param  finished
*/

void progress(bool finished)
{
	static float tick = 0.0;
	if(finished) tick = 0.9;
	int barWidth = 70;
	int pos = barWidth * tick;
	
	if(!finished){
		std::cout << "\033[1;33mprocessing [";
		for (int i = 0; i < barWidth-6; ++i) {
			if (i < pos) std::cout << " ";
			else if (i == pos) std::cout << ">>>";
			else std::cout << " ";
		}
	} 
	else // finished
	{
		std::cout << "\033[1;32m \u2713 Done!   [";
		for (int i = 0; i < barWidth-6; ++i) {
			if (i < pos) std::cout << ">";
			else if (i == pos) std::cout << ">>>";
			else std::cout << " ";
		}
	}
	
	std::cout << "] "  << "\r\033[0m";
	std::cout.flush();
	
	tick += 0.1; // for demonstration only
	if (tick > 0.95) tick = 0.0;
}

/**
	@brief   splits a line into vector elems by using a delimiter symbol
	@param   line, delim, elems
	@retval  none 
*/

void split(string line, const string delim,vector<string>& elems){
	
	size_t last = 0; 
	size_t next = 0; 
	
		// split a row in substr containing more or less xml attributes
	while ((next = line.find(delim, last)) != string::npos) 
	{ 
		// save all substrings in a vector
		elems.push_back( line.substr(last, next-last) );
		last = next + 1; 
	} 
	elems.push_back( line.substr(last) );
}

int main(int argc, char* argv[]){
	
	int reputation;
		// cli flag handling
	if (argc > 1) // extract cli arguments
	{
		try // extract score
		{
			// Process a -reputation=x flag as first argument
			string reputationArg(argv[1]);
			reputation = stoi(reputationArg.substr(12,string::npos));
		}
		catch(std::invalid_argument& e){
			cout << "\033[1;31m\u26C8 error: you have chosen an invalid flag! Use -reputation=NUM instead!\033[0m" << endl;
			cout << "\033[1;31m\u26C8 CommentFilter exited due to an error \033[0m" << endl;
			return 1;
		}
		catch(std::out_of_range){
			cout << "\033[1;31m\u26C8 error: you have chosen an invalid flag! Use -reputation=NUM instead!\033[0m" << endl;
			cout << "\033[1;31m\u26C8 CommentFilter exited due to an error \033[0m" << endl;
		}
	}
	else
	{
		reputation = 10;
		cout << "\033[1;33mNot enough flags for CommentFilter found, fall back to default values!\033[0m" << endl;
		cout << "\033[1;33mFlags -reputation=NUM can be defined in the script!\033[0m\n" << endl;
	}
	
	
	auto start = high_resolution_clock::now(); // start timer
	ifstream fileInput(inFileName);
	
	if(fileInput)
	{
		ofstream trimmed_stream_out(trimmed_users_path);					// open outputs
		ofstream id_stream_out(deleted_ids_path);
		
			#ifdef DEBUG
				if(trimmed_stream_out){cout << "trimmed_stream_out open" << endl;};
				if(trimmed_stream_out){cout << "id_stream_out open" << endl;};
			#endif
				
		string line;
		
		//substrings
		vector<string> elems;
		
		int cur_line = 1.0;
		cout << "\033[1;33mStart trimming process...\033[0m" << endl;
		sleep(1);
		while(getline( fileInput, line )){ 								// process line
			cur_line++; 
			
				#ifdef DEBUG
					cout << "current line: " << line << " number: " << cur_line  << endl;
				#endif
			
			if (cur_line%50000 == 0)
			{
				progress(false);
			}
			
			split(line,delim, elems);
			
			if (elems.size() < 3) 										// ignore header footer
			{
				elems.clear();
				trimmed_stream_out << line << endl;						// adopt lines without change
				continue;
			} 
			
			string ScoreString = elems[1].substr(13,string::npos); 		//extract the reputation number with format Reputation="xxx from elems vector
			
			if ( stoi(ScoreString) >= reputation )
			{		
				trimmed_stream_out << line << endl;
			} 
			else														// lines with score < Limit
			{
				string IDstring = elems[0].substr(11,string::npos); 		//extract the ID number with format <row Id="xxxx from elems vector
				id_stream_out << IDstring << endl;						// // write Ids to file
			}
			
			elems.clear();
		}
		
		//close input
		fileInput.close();
		trimmed_stream_out.close();
		id_stream_out.close();
		
		auto stop = high_resolution_clock::now(); //stop timer
		auto duration = duration_cast<seconds>(stop - start); // calc time difference
		auto duration2 = duration_cast<minutes>(stop - start); // calc time difference
		progress(true);
		cout << endl;
		cout << "\033[1;33mtrimmed users\033[0m in " << duration.count() << " seconds"  << " approx. " << duration2.count() << " minutes"<< endl; // time output
	}
	
	return 0;
	
}

