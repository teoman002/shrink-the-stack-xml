/**
    PostsFilter.cpp
    Purpose: Trims Posts.xml by using the xml attributes "score" "viewcount" "favcount". All xml lines with an attribute smaller than the passed parameters will be omitted and their "PostID" will be written into a seperate file.

    @author Teoman Reimann
    @version 2.0 20/07/19
    @arguments  -score=NUM -viewcount=NUM -favcount=NUM
*/

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <chrono> 		// measure time
#include <unistd.h>

using namespace std;
using namespace chrono;

///////////////////////////CONFIG/////////////////////////////////
#define STORE_ACCEPTED 1;
const string inFileName = "./Posts_src.xml";
const string trimmed_posts_path = "./Posts.xml";
const string deleted_ids_path = "outputs/Posts_deleted_ids.xml";
const string delim = "\" "; // Used to split xml attributes
//////////////////////////////////////////////////////////////////


/**
	@brief  shows a dynamic progress bar in the terminal
	@param  finished
*/

void progress(bool finished)
{
	static float tick = 0.0;
	if(finished) tick = 0.9;
	int barWidth = 70;
	int pos = barWidth * tick;
	
	if(!finished){
		std::cout << "\033[1;33mprocessing [";
		for (int i = 0; i < barWidth-6; ++i) {
			if (i < pos) std::cout << " ";
			else if (i == pos) std::cout << ">>>";
			else std::cout << " ";
		}
	} 
	else //finished
	{
		std::cout << "\033[1;32m \u2713 Done!   [";
		for (int i = 0; i < barWidth-6; ++i) {
			if (i < pos) std::cout << ">";
			else if (i == pos) std::cout << ">>>";
			else std::cout << " ";
		}
	}
	
	std::cout << "] "  << "\r\033[0m";
	std::cout.flush();
	
	//std::cout << std::endl;
	tick += 0.1; // for demonstration only
	if (tick > 0.95) tick = 0.0;
}

/**
	@brief   splits a line into vector elems by using a delimiter symbol
	@param   line, delim, elems
	@retval  none 
*/

void split(const string line, const string delim,vector<string>& elems){
	
	size_t last = 0; 
	size_t next = 0; 
	
	// Note: all words of body starting with (" ) create extra string elements in the vector.
	// Split a row in substr containing more or less xml attributes
	while ((next = line.find(delim, last)) != string::npos) 
	{ 
		// Save all substrings in a vector
		elems.push_back( line.substr(last, next-last) );
		last = next + 1; 
	} 
	elems.push_back( line.substr(last) );
}

/**
	@brief   finds the score attribute in elems vector and returns the index
	@param   elems, cur_line
	@retval  index
*/

int const find_score_idx(vector<string>& elems, size_t& cur_line) 
{
	// Ocassionally the score attribute isn't the fourth attribute, so the program has to search the index of the score attribute in the vector.
	int xml_elem_idx_frScore = 3;
	
	// Check if the letters from score= are at a specific index in the vector, if not increase the index.
	while (elems[xml_elem_idx_frScore].at(1) != 'S' && elems[xml_elem_idx_frScore].at(5) != 'e' && elems[xml_elem_idx_frScore].at(6) != '=')
		xml_elem_idx_frScore++;
	
	//WARNING
	/*
	- it is possible that the body or title have a word inside,
		which would throw an exception due to bad format of the score attribute 
	- this magic word has the form: (" S***e=), asteriks could be anything
	- stoi(ScoreString) will throw an error in that case
	- will be addressed when considered an issue
	*/
	
	// Score attribute not found exception
	if	(xml_elem_idx_frScore+1 > elems.size())
		{
			cout << "check vector for score attribute at index: " << xml_elem_idx_frScore << endl;
			cout << "Index is bigger than size of vector: " << elems.size() << endl;
			try{throw 1;} catch (int e)
			{
				cout << "\033[1;31m\u26C8 error: score attribute not found!\033[0m" << endl;
				cout << "\033[1;31m\u26C8 PostFilter exited due to an error \033[0m" << endl;
				return 1;
			}
		}
		
	return xml_elem_idx_frScore;
}

/**
	@brief   finds the viewcount attribute in elems vector and returns the index
	@param   elems, cur_line
	@retval  index
*/

int const find_view_count_idx(vector<string>& elems, size_t& cur_line) 
{
	// Ocassionally the viewcount attribute isn't the fifth attribute, so the program has to search for it the vector.
	int xml_elem_idx_fr_vw_cnt = 4;
	
	// Check if the letters from score= are at a specific index in the vector, if not increase the index.
	while (xml_elem_idx_fr_vw_cnt < 6 && elems[xml_elem_idx_fr_vw_cnt].at(1) != 'V' && elems[xml_elem_idx_fr_vw_cnt].at(4) != 'w' && elems[xml_elem_idx_fr_vw_cnt].at(5) != 'C')
		xml_elem_idx_fr_vw_cnt++;
	
	if (xml_elem_idx_fr_vw_cnt == 6)
		return 0; // viewcount is never at position 0 -> not found
	else
		return xml_elem_idx_fr_vw_cnt;
}

/**
	@brief   finds the favcount attribute in elems vector and returns the index
	@param   elems, cur_line
	@retval  index
*/

int const find_fav_count_idx(vector<string>& elems, size_t& cur_line) 
{
	// no fav count found;
	if(elems.size() < 15) return 0;
	
	// Check if the letters from score= are at a specific index in the vector, if not increase the index.
	if (elems[14].at(1) == 'F' && elems[14].at(6) == 'i' && elems[14].at(9) == 'C')
	{
		#ifdef DEBUG
			cout << "accepted answer found " << elems[14] << " at line " << cur_line << endl;
		#endif
		return 14;
	}
	
	return 0;
}

/**
	@brief   checks if the current line has an accepted answer attribute
	@param   elems, cur_line
	@retval  bool
*/

bool const is_accepted_answer(vector<string>& elems, size_t& cur_line) 
{
	// Check if the letters from A***p***AnswerId= are at index 2 in the vector.
	if (elems[2].at(1) == 'A' && elems[2].at(5) == 'p' && elems[2].at(9) == 'A')
	{
		#ifdef DEBUG
			cout << "accepted answer found " << elems[2] << " at line " << cur_line << endl;
		#endif
		return true;
	}
	else
		return false;
}

/**
	@brief   stores a line in the output file, ommit a line and write its id in another output file
	@param   bool store, trimmed_stream_out, id_stream_out, line, elems
	@retval  none
*/

void store_line(bool store, ofstream& trimmed_stream_out, ofstream& id_stream_out,const string& line,const vector<string>& elems)
{
	if (store)
	{	
		trimmed_stream_out << line << endl;
	} 
	else														// Lines with score < Limit
	{
		string IDstring = elems[0].substr(11,string::npos); 	// Extract the ID number with format <row Id="xxxx from elems vector
		id_stream_out << IDstring << endl;						// Write Ids to file
	}
}

int main(int argc, char* argv[])
{
	int score_limit;
	int view_count_limit;
	int fav_count_limit;
	
	// cli flag handling
	if (argc > 3) // extract cli arguments
	{
		try // Extract score
		{
			// Process a -score=x flag as first argument
			string scoreArg(argv[1]);
			score_limit = stoi(scoreArg.substr(7,string::npos));
			
			// Process -viewcount=x flag as second argument
			string viewCountArg(argv[2]);
			view_count_limit = stoi(viewCountArg.substr(11,string::npos));
			
			// Process -favcount=x flag as second argument
			string favcountArg(argv[3]);
			fav_count_limit = stoi(favcountArg.substr(10,string::npos));
			
		}
		catch(std::invalid_argument& e){
			cout << "\033[1;31m\u26C8 error: you have chosen an invalid flag!\033[0m" << endl;
			cout << "\033[1;33mFlags -score=NUM -viewcount=NUM -favcount=NUM can be defined in the starter script!\033[0m\n" << endl;
			cout << "\033[1;31m\u26C8 PostFilter exited due to an error \033[0m" << endl;
			return 1;
		}
		catch(std::out_of_range){
			cout << "\033[1;31m\u26C8 error: you have chosen an invalid flag!\033[0m" << endl;
			cout << "\033[1;33mFlags -score=NUM -viewcount=NUM -favcount=NUM can be defined in the starter script!\033[0m\n" << endl;
			cout << "\033[1;31m\u26C8 PostFilter exited due to an error \033[0m" << endl;
			return 1;
		}
	} 
	else
	{
		// Posts with a score below 4 will be deleted
		score_limit = 4;
		view_count_limit = 5000;
		fav_count_limit = 2;
		cout << "\033[1;33mNot enough flags for PostFilter found, fall back to default values!\033[0m" << endl;
		cout << "\033[1;33mFlags -score=NUM -viewcount=NUM -favcount=NUM can be defined in the script!\033[0m\n" << endl;
	}
	
	
	
	auto start = high_resolution_clock::now(); // Start timer
	ifstream fileInput(inFileName);
	
	if(fileInput)
	{
		// Define helper vars
		string line;					// Current xml line
		vector<string> elems;			// Current attributes in a xml line
		size_t cur_line = 1.0;			// For progress
		size_t unknown_format_cnt = 0;				// Count lines with unknown format;
		
		// Open outputs
		ofstream trimmed_stream_out(trimmed_posts_path); 	// New xml file
		ofstream id_stream_out(deleted_ids_path);			// Deleted ids file
		
		#ifdef DEBUG
			if(trimmed_stream_out){cout << "trimmed_stream_out is open" << endl;};
			if(trimmed_stream_out){cout << "id_stream_out is open" << endl;};
		#endif
		
		// Info in the terminal
		cout << "\033[1;33mStart trimming process...\033[0m" << endl;
		sleep(1);
		
		// Process each line of the xml
		while(getline( fileInput, line )){
			cur_line++; 
			
			#ifdef DEBUG
				//cout << "current line: " << line << " number: " << cur_line  << endl;
				//cout << "cur_line: "<< cur_line << endl;
			#endif
			
			if (cur_line%50000 == 0)
			{
				//std::cout << "line: " << line <<std::endl;
				progress(false);
			}
			
			// Split a xml line into xml attributes
			split(line,delim, elems);
			
			// Headers and footers are irrelevant, adopt them without change
			if (elems.size() < 3)
			{
				trimmed_stream_out << line << endl;
				elems.clear();
				continue;
			}
			
			
			
			// Trimming with viewcount attribute
			int xml_elem_idx_fr_vw_cnt = find_view_count_idx(elems, cur_line);
			
			// if viewcount does not exist, assume condition met
			bool has_enough_views = true;
			int view_count = 0; // just needs a value
			
			if (xml_elem_idx_fr_vw_cnt)
			{
				// Extract the viewcount number with format ViewCount="xxxx from elems vector
				string ViewCountString = elems[xml_elem_idx_fr_vw_cnt].substr(12,string::npos);
				
				try // converting strings to ints
				{
					view_count = stoi(ViewCountString);
					has_enough_views = ( view_count >= view_count_limit );
				}
				catch(std::invalid_argument& e){ //otherwise ignore views
					unknown_format_cnt++;
				}
			}
			
			// Store line immediately when accepted AcceptedAnswerId exists and go to next line
			#ifdef STORE_ACCEPTED
			if (is_accepted_answer(elems, cur_line) && has_enough_views)
			{
				store_line(true, trimmed_stream_out, id_stream_out, line,elems);
				elems.clear();
				continue;
			}
			#endif
			
			
			
			// go to next line if post has not enough views
			if (!has_enough_views)
			{
				#ifdef DEBUG
					cout << "delete line " << cur_line << " for having too few views"<< endl;
				#endif
				store_line(false, trimmed_stream_out, id_stream_out, line,elems);
				elems.clear();
				continue;
			}
			
			// if fav_count does not exist, assume condition not met
			int xml_elem_idx_fr_fav_cnt = find_fav_count_idx(elems, cur_line);
			string favCountString("00");
			if (xml_elem_idx_fr_fav_cnt)
			{
				favCountString = elems[xml_elem_idx_fr_fav_cnt].substr(16,string::npos);
			}
			
			// trimming with score attribute
			int xml_elem_idx_frScore = find_score_idx(elems,cur_line);
			// extract the score number with format Score="xxx from elems vector
			string ScoreString = elems[xml_elem_idx_frScore].substr(8,string::npos);
			
			int score_int;
			int fav_count_int;
			
			
			try // converting strings to ints
			{
				score_int = stoi(ScoreString);
				fav_count_int = stoi(favCountString);
			}
			catch(std::invalid_argument& e){ //otherwise store line due to unknown format
				unknown_format_cnt++;
				store_line(true, trimmed_stream_out, id_stream_out, line,elems);
				elems.clear();
				continue;
			}
			
			if ( score_int >= score_limit || fav_count_int >= fav_count_limit)
			{
				store_line(true, trimmed_stream_out, id_stream_out, line,elems);
			}
			
			elems.clear();
		}
		
		// Close all filestreams
		fileInput.close();
		trimmed_stream_out.close();
		id_stream_out.close();
		
		auto stop = high_resolution_clock::now(); // Stop timer
		auto duration = duration_cast<seconds>(stop - start); // Calc time difference
		auto duration2 = duration_cast<minutes>(stop - start); // Calc time difference
		progress(true);
		cout << endl;
		cout << "\033[1;33mtrimmed posts\033[0m in " << duration.count() << " seconds"  << " approx. " << duration2.count() << " minutes"<< endl; // Time output
		
		if (unknown_format_cnt) // warning if necessary
			cout << "\033[1;33m" << unknown_format_cnt << " lines had an unknown format which was ignored\033[0m" << endl; 
	}
	
	return 0;
}
