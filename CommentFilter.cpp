
/**
    CommentFilter.cpp
    Purpose: Trims Comments.xml by using the xml attribute "score". All entries with a score smaller than the passed parameter will be omitted and their "PostID" will be written into a seperate file.

    @author Teoman Reimann
    @version 1.0 20/07/19
    @arguments  -score=NUM

*/

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <chrono> 
#include <unistd.h>

using namespace std::chrono; 
using namespace std;

const string inFileName = "./Comments_src.xml";
const string trimmed_comments_path = "./Comments.xml";
const string deleted_ids_path = "outputs/Comments_deleted_ids.xml";
const string delim = "\" "; //used to split xml attributes


/**
	@brief  shows a dynamic progress bar in the terminal
	@param  finished
*/

void progress(bool finished)
{
	static float tick = 0.0;
	if(finished) tick = 0.9;
	int barWidth = 70;
	int pos = barWidth * tick;
	
	if(!finished){
		std::cout << "\033[1;33mprocessing [";
		for (int i = 0; i < barWidth-6; ++i) {
			if (i < pos) std::cout << " ";
			else if (i == pos) std::cout << ">>>";
			else std::cout << " ";
		}
	} 
	else // finished
	{
		std::cout << "\033[1;32m \u2713 Done!   [";
		for (int i = 0; i < barWidth-6; ++i) {
			if (i < pos) std::cout << ">";
			else if (i == pos) std::cout << ">>>";
			else std::cout << " ";
		}
	}
	
	std::cout << "] "  << "\r\033[0m";
	std::cout.flush();
	
	// std::cout << std::endl;
	tick += 0.1; // for demonstration only
	if (tick > 0.95) tick = 0.0;
}

/**
	@brief   splits a line into vector elems by using a delimiter symbol
	@param   line, delim, elems
	@retval  none 
*/

void split(string line, const string delim,vector<string>& elems){
	
	size_t last = 0; 
	size_t next = 0; 
	
	// split a row in substr containing more or less xml attributes
	while ((next = line.find(delim, last)) != string::npos) 
	{ 
		// save all substrings in a vector
		elems.push_back( line.substr(last, next-last) );
		last = next + 1; 
	} 
	elems.push_back( line.substr(last) );
}

int main(int argc, char* argv[]){
	
	int score_limit;
		// cli flag handling
	if (argc > 1) // extract cli arguments
	{
		try // extract score
		{
			// Process a -score=x flag as first argument
			string scoreArg(argv[1]);
			score_limit = stoi(scoreArg.substr(7,string::npos));
		}
		catch(std::invalid_argument& e){
			cout << "\033[1;31m\u26C8 error: you have chosen an invalid flag! Use -score=NUM instead!\033[0m" << endl;
			cout << "\033[1;31m\u26C8 CommentFilter exited due to an error \033[0m" << endl;
			return 1;
		}
		catch(std::out_of_range){
			cout << "\033[1;31m\u26C8 error: you have chosen an invalid flag! Use -score=NUM instead!\033[0m" << endl;
			cout << "\033[1;31m\u26C8 CommentFilter exited due to an error \033[0m" << endl;
			return 1;
		}
	}
	else
	{
		score_limit = 3;
		cout << "\033[1;33mNot enough flags for CommentFilter found, fall back to default values!\033[0m" << endl;
		cout << "\033[1;33mFlags -score=NUM can be defined in the script!\033[0m\n" << endl;
	}
	
	auto start = high_resolution_clock::now(); // start timer
	ifstream fileInput(inFileName);
	
	if(fileInput)
	{
		ofstream trimmed_stream_out(trimmed_comments_path);					// open outputs
		ofstream id_stream_out(deleted_ids_path);
		
			#ifdef DEBUG
				if(trimmed_stream_out){cout << "trimmed_stream_out open" << endl;};
				if(trimmed_stream_out){cout << "id_stream_out open" << endl;};
			#endif
				
		string line;
		
		//substrings
		vector<string> elems;
		
		int cur_line = 1.0;
		cout << "\033[1;33mStart trimming process...\033[0m" << endl;
		sleep(1);
		while(getline( fileInput, line )){ 								// process line
			cur_line++; 
			
				#ifdef DEBUG
					cout << "current line: " << line << " number: " << cur_line  << endl;
				#endif
			
			if (cur_line%50000 == 0)
			{
				static bool finished = false;
				progress(finished);
			}
			
			split(line,delim, elems);
			
			if (elems.size() < 3) 										//ignore header footer
			{
				elems.clear();
				trimmed_stream_out << line << endl;						// adopt lines without change
				continue;
			} 
			
			string ScoreString = elems[2].substr(8,string::npos); 		// extract the score number with format Score="xxx from elems vector
			
			if ( stoi(ScoreString) >= score_limit )
			{		
				trimmed_stream_out << line << endl;
			} 
			else														// lines with score < Limit
			{
				string IDstring = elems[1].substr(9,string::npos); 		// extract the ID number with format PostId="xxx from elems vector
				id_stream_out << IDstring << endl;						// write Ids to file
			}
			
			elems.clear();
		}

		//close input
		fileInput.close();
		trimmed_stream_out.close();
		id_stream_out.close();
		
		auto stop = high_resolution_clock::now(); //stop timer
		auto duration = duration_cast<seconds>(stop - start); // calc time difference
		auto duration2 = duration_cast<minutes>(stop - start); // calc time difference
		progress(true);
		cout << endl;
		cout << "\033[1;33mtrimmed comments\033[0m in " << duration.count() << " seconds"  << " approx. " << duration2.count() << " minutes"<< endl; 
	}
	
	return 0;
	
}

