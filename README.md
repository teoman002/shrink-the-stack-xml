# shrink-the-stack-xml

## Purpose
shrinks xml databases of stackexchange.com to allow users to run an lightweight offline instance with the aid of stackdump

With the help of the xml attributes "score" "viewcount" "favcount" "reputation", it is possible to delete posts,comments and users that aren't popular. Hence there is a way to safe disk space.
The data dump of Stackoverflow.com takes up more than 100 GB disk space. A library that big is for many not worth the space.
Fortunately the size can be minimized more than 80% by applying a filter that deletes all entries that i.e. have a score less than 4. That limit can be changed in the setup_script_*, 
however tests have shown that this number provides a good balance between safed disk-space and quality of answers.
Feel free to change it to 5 for less or to 3 for more disk-space.

## Additional dependencies
[stackdump](https://bitbucket.org/samuel.lai/stackdump)

```
wget, g++,7z
```
## Building
### Setup on Ubuntu / Mint / Debian

Download all the files and put them into the root folder of stackdump.
Make the setup_script.sh executable and start it in a terminal.
```bash
chmod +x setup_script_stackoverflow.sh
./setup_script_stackoverflow.sh
```

Next open two more terminals for the local stackexchange instance:

Terminal 1 runs the indexer:
```bash
./start_solr.sh
```

Terminal 2 is parsing the database:
```bash
dateVar=`date +"%m %Y"`
./manage.sh import_site --base-url stackoverflow.com --dump-date "$dateVar" ./dumpData
#start the web-application
./start_web.sh
```

Finally connect to your server on `localhost:8080`

## Stackdump is outdated, importing not working anymore
Using the default settings,the imported **xml data** had a **size of 5GB**.
The **RAM consuption** was **above 40 GB**, which prevented me from importing it.
Stackdump needs to be rewritten such that the data is stored on disk temporarily.
Unfortunetely the dev abandoned the project and someone new with knowledge about databases is required.
The program is written in python. C++ would be a lot faster with the parsing, however there is no C++ client for Apache Solr.
Java seems to be an option. According to the issue section of stackdump the xml parsing and sql database performance can be improved a lot.

## Credits
Thanks for Samuel and his work on stackdump.

## License
shrink-the-stack is licensed under the [MIT License](https://en.wikipedia.org/wiki/MIT_License).
